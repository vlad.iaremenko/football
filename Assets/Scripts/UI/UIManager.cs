﻿using System;
using Football.EventsSystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Football.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject _startPanel;
        [SerializeField] private GameObject _gameOverPanel;
        [SerializeField] private Button _startButton;
        [SerializeField] private Button _restartButton;
        
        private SignalBus _signalBus;
        
        [Inject]
        private void Inject(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }
        
        private void Awake()
        {
            _startButton.onClick.AddListener(StartGame);
            _restartButton.onClick.AddListener(Restart);
            
            _signalBus.Subscribe<GameOverEvent>(GameOver);
        }

        private void GameOver()
        {
            _gameOverPanel.SetActive(true);
        }

        private void OnDestroy()
        {
            _startButton.onClick.RemoveAllListeners();
            _restartButton.onClick.RemoveAllListeners();
            _signalBus.Unsubscribe<GameOverEvent>(GameOver);
        }

        private void Restart()
        {
            SceneManager.LoadScene(0);
        }
        
        private void StartGame()
        {
            _signalBus.Fire<StartGameEvent>();
            _startPanel.gameObject.SetActive(false);
        }
    }
}
