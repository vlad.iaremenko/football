using System;
using UnityEngine;
using Zenject;

namespace Football.Installers
{
    [CreateAssetMenu(fileName = "GameData", menuName = "Installers/GameData")]
    public class GameDataInstaller : ScriptableObjectInstaller<GameDataInstaller>
    {
        public GameData PlayerData;
        
        [Serializable]
        public class GameData
        {
            public float Speed;
            public float TargetOffset;
            public float TargetOffsetStep;
            public float MinOffset;
            public float MaxOffset;
        }
        
        public override void InstallBindings()
        {
            Container.BindInstance(PlayerData);
        }
    }
}