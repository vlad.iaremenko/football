using Football.EventsSystem;
using Football.Player;
using UnityEngine;
using Zenject;

namespace Football.Installers
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private PlayerMoveController _playerMoveController;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<InputListener>().AsSingle();
            Container.BindInstances(_playerMoveController);

            BindSignals();
        }

        private void BindSignals()
        {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<StartGameEvent>();
            Container.DeclareSignal<GameOverEvent>();
        }
    }
}