﻿using System;
using Football.Installers;
using UnityEngine;
using Zenject;

namespace Football.Player
{
    public class PlayerMoveController : MonoBehaviour
    {
        private InputListener _inputListener;
        private Vector3 _moveDirection;
        private GameDataInstaller.GameData _gameData;
        private Action _moveAction;
        private Rigidbody _rigidbody;
        
        [Inject]
        private void Inject(InputListener inputListener, GameDataInstaller.GameData gameDataInstaller)
        {
            _inputListener = inputListener;
            _gameData = gameDataInstaller;
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            _moveAction?.Invoke();
        }

        private void Move()
        {
            _rigidbody.velocity = _inputListener.InputDirection * _gameData.Speed;
        }
        
        public void SetMoveMode(MoveState state)
        {
            switch (state)
            {
                case MoveState.Stop:
                    _moveAction = Stop;
                    break;
                case MoveState.Move:
                    _moveAction = Move;
                    break;
            }
        }

        private void Stop()
        {
            _rigidbody.velocity = Vector3.zero;;
        }
    }
    
    public enum MoveState
    {
        Move,
        Stop
    }
}