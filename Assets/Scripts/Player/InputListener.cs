﻿using UnityEngine;
using Zenject;

namespace Football.Player
{
    public class InputListener : ITickable
    {
        private readonly string _vertical = "Vertical";
        private readonly string _horizontal = "Horizontal";
        
        private Vector3 _direction;
        public Vector3 InputDirection => _direction;
        
        public void Tick()
        {
            _direction.x = Input.GetAxis(_horizontal);
            _direction.z = Input.GetAxis(_vertical);
        }
    }
}
