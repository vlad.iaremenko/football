﻿using Football.EventsSystem;
using UnityEngine;
using Zenject;

namespace Football.Player
{
    public class PlayerStateController : MonoBehaviour
    {
        private SignalBus _signalBus;
        private PlayerMoveController _playerMoveController;

        [Inject]
        private void Inject(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        private void Awake()
        {
            _signalBus.Subscribe<StartGameEvent>(StartMoving);
            _signalBus.Subscribe<GameOverEvent>(StopMoving);
            _playerMoveController = GetComponent<PlayerMoveController>();
        }

        private void StartMoving()
        {
            _playerMoveController.SetMoveMode(MoveState.Move);
        }

        private void StopMoving()
        {
            _playerMoveController.SetMoveMode(MoveState.Stop);
        }
    }
}