﻿using System;
using Football.EventsSystem;
using Football.Installers;
using Football.Player;
using UnityEngine;
using Zenject;

namespace Football.Enemy
{
    public class EnemyMoveController : MonoBehaviour
    {
        [SerializeField] private float _speed = 1;

        private PlayerMoveController _playerMoveController;
        private GameDataInstaller.GameData _playerData;

        private Vector3 _direction = Vector3.zero;
        private Vector3 _attackZOffset = Vector3.zero;
        private Vector3 _maxDistanceOffset = Vector3.zero;

        private Action _moveAction;

        private Rigidbody _rigidbody;

        [Inject]
        private void Inject(PlayerMoveController playerMoveController, GameDataInstaller.GameData playerData)
        {
            _playerMoveController = playerMoveController;
            _playerData = playerData;
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            CalculateStartAttackOffset();
        }

        //Calculated attack position depending on difference of speed between enemy and player
        private void CalculateStartAttackOffset()
        {
            _attackZOffset.z = _playerData.Speed - _speed;
            
            CheckMinDistance();

            _attackZOffset.z *= _playerData.TargetOffset;
            
            _attackZOffset.z =  Mathf.Clamp(_attackZOffset.z, _playerData.MinOffset, _playerData.MaxOffset);
            
            _maxDistanceOffset.z = _attackZOffset.z;
        }

        //Update attack position when getting closer or further away from target
        private void CheckDistanceToTarget()
        {
            if (Mathf.Abs(transform.position.z - _playerMoveController.transform.position.z) <= _attackZOffset.z)
                _attackZOffset.z -= _playerData.TargetOffsetStep;

            if (transform.position.z > _attackZOffset.z + _playerMoveController.transform.position.z + 1f)
                if (transform.position.z < _maxDistanceOffset.z + _playerMoveController.transform.position.z)
                {
                    _attackZOffset.z += _playerData.TargetOffsetStep;
                    
                    if (_attackZOffset.z > _maxDistanceOffset.z)
                        _attackZOffset.z = _maxDistanceOffset.z;
                }

            CheckMinDistance();
        }

        private void CheckMinDistance()
        {
            if (transform.position.z < _playerMoveController.transform.position.z)
                _attackZOffset.z = 0f;
        }

        private void FixedUpdate()
        {
            _moveAction?.Invoke();
        }

        private void Move()
        {
            _direction = (_playerMoveController.transform.position + _attackZOffset - transform.position).normalized;
            _rigidbody.velocity = _direction * _speed;
        }

        private void Stop()
        {
            _rigidbody.velocity = Vector3.zero;;
        }

        public void SetMoveMode(MoveState state)
        {
            switch (state)
            {
                case MoveState.Stop:
                    _moveAction = Stop;
                    break;
                case MoveState.Move:
                    _moveAction = () =>
                    {
                        CheckDistanceToTarget();
                        Move();
                    };
                    break;
            }
        }

        //Draw current target position
        /*private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            if (_playerMoveController == null)
                return;

            Gizmos.DrawSphere(_playerMoveController.transform.position + _attackZOffset, 0.5f);

            Gizmos.color = Color.green;

            Gizmos.DrawSphere(_playerMoveController.transform.position + _maxDistanceOffset, 0.5f);
        }*/
    }
}