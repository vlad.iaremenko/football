﻿using System;
using Football.EventsSystem;
using Football.Installers;
using Football.Player;
using UnityEngine;
using Zenject;

namespace Football.Enemy
{
    public class EnemyStateController : MonoBehaviour
    {
        private SignalBus _signalBus;
        private EnemyMoveController _enemyMoveController;
        
        [Inject]
        private void Inject(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        private void Awake()
        {
            _signalBus.Subscribe<StartGameEvent>(StartMoving);
            _signalBus.Subscribe<GameOverEvent>(StopMoving);
            _enemyMoveController = GetComponent<EnemyMoveController>();
        }

        private void StartMoving()
        {
            _enemyMoveController.SetMoveMode(MoveState.Move);
        }
        
        private void StopMoving()
        {
            _enemyMoveController.SetMoveMode(MoveState.Stop);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out PlayerMoveController component))
            {
                _signalBus.Fire<GameOverEvent>();
            }
        }
        
        private void OnDestroy()
        {
            _signalBus.Unsubscribe<StartGameEvent>(StartMoving);
            _signalBus.Unsubscribe<GameOverEvent>(StopMoving);
        }
    }
}
