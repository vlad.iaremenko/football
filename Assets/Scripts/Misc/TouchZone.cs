﻿using System;
using Football.EventsSystem;
using Football.Player;
using UnityEngine;
using Zenject;

namespace Football.Misc
{
    public class TouchZone : MonoBehaviour
    {
        private SignalBus _signalBus;

        [Inject]
        private void Inject(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out PlayerMoveController playerMoveController))
            {
                _signalBus.Fire<GameOverEvent>();
            }
        }
    }
}
